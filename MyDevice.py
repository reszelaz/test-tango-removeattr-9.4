import time

import tango
from tango import Attr, AttrWriteType
from tango.server import LatestDeviceImpl, DeviceClass

class MyDevice(LatestDeviceImpl):


    def init_device(self):
        super().init_device()
        self.set_state(tango.DevState.ON)
    
    def initialize_dynamic_attributes(self):
        attr_name = "Attr1"
        attr_type = tango.DevDouble
        
        device_attr_names = []
        multi_attr = self.get_device_attr()
        for i in range(multi_attr.get_attr_nb()):
            device_attr_names.append(multi_attr.get_attr_by_ind(i).get_name())
        
        if attr_name in device_attr_names:
            print("remove device attribute: {}".format(attr_name))
            self.remove_attribute(attr_name)
        attr = Attr(attr_name, attr_type, AttrWriteType.READ_WRITE)
        self.add_attribute(attr, self.read_Attr1, self.write_Attr1)
    
    def read_Attr1(self, attr):
        attr_name = attr.get_name()
        self.info_stream("Reading {}".format(attr_name))
        attr.set_value(11.11)
    
    def write_Attr1(self, attr):
        self.info_stream("Writting {}".format(attr.get_name()))


class MyDeviceClass(DeviceClass):

    def __init__(self, name):
        super().__init__(name)
        self.set_type(name)

    def dyn_attr(self, dev_list):
        for dev in dev_list:
            try:
                dev.initialize_dynamic_attributes()
            except Exception as e:
                print("Failed to initialize dynamic attributes")
                print(e)
