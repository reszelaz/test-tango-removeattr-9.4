# Demo to show problems with removing attributes in Tango 9.4

We hit this issue in the Sardana project when trying it with PyTango 9.4
release candidates.

More information in: https://gitlab.com/sardana-org/sardana/-/issues/1810

## Environment

This issue happens with the following conda environment:
```
conda create -n sardana_pytango_rc2 -c conda-forge/label/pytango_rc -c conda-forge python=3.10 pytango=9.4.0rc2
```

It works correctly with:

```
conda create -n sardana_pytango_stable -c conda-forge python=3.10 pytango
```

## Steps to reproduce

1. Register in Tango Database one `DeviceServer` DS with instance name `test`
   with:
   - 1 device of `PoolDevice` class, with the following name: `test/pooldevice/1`

    ```console
    tango_admin --add-server DeviceServer/test PoolDevice test/pooldevice/1
    ```
2. Start `DeviceServer`: `python3 DeviceServer.py test`
3. Start client: `python3 client.py`

## Result

`DeviceServer` reports the following error when trying to create third device 
of `MyDevice` class:

```
Ready to accept request
remove device attribute: Attr1
remove device attribute: Attr1
Failed to initialize dynamic attributes
DevFailed[
DevError[
    desc = Device --> a/b/c
           Property writable_attr_name for attribute attr1 is set to Attr1, but this attribute does not exist
  origin = void Tango::MultiAttribute::check_associated(long int, const string&) at (/home/conda/feedstock_root/build_artifacts/cpptango_1667919173430/work/cppapi/server/multiattribute.cpp:570)
  reason = API_AttrOptProp
severity = ERR]

DevError[
    desc = Attribute Attr1 is not defined as attribute for your device.
           Can't remove it
  origin = void Tango::DeviceImpl::remove_attribute(const string&, bool, bool) at (/home/conda/feedstock_root/build_artifacts/cpptango_1667919173430/work/cppapi/server/device.cpp:3784)
  reason = API_AttrNotFound
severity = ERR]
]
```
