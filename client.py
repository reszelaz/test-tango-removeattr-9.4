import tango

pool = tango.DeviceProxy("test/pooldevice/1")
for i in range(1, 4):
    print("create device {}".format(i))
    pool.CreateDevice(["MyDevice", "test/mydevice/{}".format(i)])
